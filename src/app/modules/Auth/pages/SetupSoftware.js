import React from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import { connect } from "react-redux";
import * as auth from "../_redux/authRedux";
import { Link, useHistory } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";

const initialValues = {

};

function SetupSoftware(props) {

	const SetupSoftwareSchema = Yup.object().shape({

	});

	let history = useHistory();

	const formik = useFormik({
    initialValues,
    validationSchema: SetupSoftwareSchema,
    onSubmit: (values, { setStatus, setSubmitting }) => {
			history.push("/auth/tutorial");
    },
	});

	return (
		<div className="login-form login-sign" style={{ display: "block" }}>
			<div className="text-center mb-10 mb-lg-20">
				<h3 className="font-size-h1">
					<FormattedMessage id="AUTH.SETUPSOFTWARE.TITLE" />
				</h3>
				<p className="text-muted font-weight-bold">
					Please download and install software.
				</p>
			</div>

			<form
				className="form fv-plugins-bootstrap fv-plugins-framework animated animate__animated animate__backInUp"
				onSubmit={formik.handleSubmit}
			>
				<div className="form-group d-flex flex-wrap flex-center">
					<button
						type="button"
						className="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4"
					>
						Download Software
					</button>
				</div>

				<div className="form-group d-flex flex-wrap flex-center">
          <Link to="/auth/makepayment">
            <button
              type="button"
              className="btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-4"
            >
              Prev
            </button>
          </Link>

          <button
            type="submit"
            className="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4"
          >
            <span>Next</span>
          </button>
        </div>
			</form>
		</div>
	)
}

export default injectIntl(connect(null, auth.actions)(SetupSoftware));
