import { connect } from "react-redux";
import React, { useState } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import * as auth from "../_redux/authRedux";
import { Link, useHistory } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";

const initialValues = {

};

function Tutorial(props) {
	const { intl } = props;
	
	const TutorialSchema = Yup.object().shape({

	});

	let history = useHistory();
	
	const formik = useFormik({
    initialValues,
    validationSchema: TutorialSchema,
    onSubmit: (values, { setStatus, setSubmitting }) => {
			history.push("/auth/complete");
    },
	});

	return (
		<div className="login-form login-sign" style={{ display: "block" }}>
			<div className="text-center mb-10 mb-lg-20">
				<h3 className="font-size-h1">
					{/* <FormattedMessage id="AUTH.MAKEPAYMENT.TITLE" /> */}
					Tutorial
				</h3>
				<p className="text-muted font-weight-bold">
					Tutorial
				</p>
			</div>

			<form
				className="form fv-plugins-bootstrap fv-plugins-framework animated animate__animated animate__backInUp"
				onSubmit={formik.handleSubmit}
			>
				<div className="form-group d-flex flex-wrap flex-center">
					<iframe src=""></iframe>
				</div>

				<div className="form-group d-flex flex-wrap flex-center">
          <Link to="/auth/setupsoftware">
            <button
              type="button"
              className="btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-4"
            >
              Prev
            </button>
          </Link>

          <button
            type="submit"
            className="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4"
          >
            <span>Next</span>
          </button>
        </div>
			</form>
		</div>
	)
}


export default injectIntl(connect(null, auth.actions)(Tutorial));
