import { connect } from "react-redux";
import React, { useState } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import * as auth from "../_redux/authRedux";
import { Link, useHistory } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";

const initialValues = {
	members: "3",
	price: "4.5",
	paymentGateway: "paypal"
};

function MakePayment(props) {
	const { intl } = props;
	const [loading, setLoading] = useState(false);

	const MakePaymentSchema = Yup.object().shape({

	});

	const enableLoading = () => {
    setLoading(true);
  };

  const disableLoading = () => {
    setLoading(false);
	};

	let history = useHistory();
	
	const formik = useFormik({
    initialValues,
    validationSchema: MakePaymentSchema,
    onSubmit: (values, { setStatus, setSubmitting }) => {
			history.push("/auth/setupsoftware");
    },
	});
	
	return (
		<div className="login-form login-sign" style={{ display: "block" }}>
			<div className="text-center mb-10 mb-lg-20">
				<h3 className="font-size-h1">
					<FormattedMessage id="AUTH.MAKEPAYMENT.TITLE" />
				</h3>
				<p className="text-muted font-weight-bold">
					Please select your plans.
				</p>
			</div>

			<form
				className="form fv-plugins-bootstrap fv-plugins-framework animated animate__animated animate__backInUp"
				onSubmit={formik.handleSubmit}
			>
				{/* begin::members */}
				<div className="form-group fv-plugins-icon-container">
					<label>MEMBERS :</label>
					<select className="form-control form-control-solid h-auto py-5 px-6">
						<option value="3">3 Users</option>
						<option value="5">5 Users</option>
						<option value="10">10 Users</option>
						<option value="20">20 Users</option>
						<option value="50">50 Users</option>
						<option value="100">100 Users</option>
					</select>
				</div>
				{/* end::members */}

				{/* begin::price */}
				<div className="form-group fv-plugins-icon-container">
					<label>PRICE :</label>
					<input 
						placeholder="Price"
						type="text"
						className="form-control form-control-solid h-auto py-5 px-6"
						name="price"
						/>
				</div>
				{/* end::price */}

				{/* begin::price */}
				<div className="form-group fv-plugins-icon-container">
					<label>PAYMENT GATEWAY :</label>
					<select className="form-control form-control-solid h-auto py-5 px-6">
						<option value="paypal">Paypal</option>
						<option value="unioncard">Union Card</option>
					</select>
				</div>
				{/* end::price */}

				<div className="form-group d-flex flex-wrap flex-center">
          <Link to="/auth/registration">
            <button
              type="button"
              className="btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-4"
            >
              Prev
            </button>
          </Link>

          <button
            type="submit"
            className="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4"
          >
            <span>Next</span>
          </button>
        </div>
			</form>
		</div>
	)
}

export default injectIntl(connect(null, auth.actions)(MakePayment));
