import React, { Component, useState } from "react";
import TeamPluseWidgetItem from "./TeamPluseWidgetItem";

const TeamPluseWidget = () => {
	const tabs = {
		tab1: "teampluse_ActiveNow",
		tab2: "teampluse_Productivity",
		tab3: "teampluse_Groups",
		tab4: "teampluse_Users"
	};

	const [activeTab, setActiveTab] = useState(tabs.tab1);

	return (
		<div className="card card-custom" style={{backgroundColor: "#825bfa"}}>
			{/* Header */}
			<div className="card-header border-0">
				<h3 className="card-title font-weight-bolder text-dark">
					Team Plus: Real time
				</h3>
				<div className="card-toolbar">
					<ul className="nav nav-pills nav-pills-sm nav-dark-75">
						<li className="nav-item">
							<a className={`nav-link py-2 px-4 ${activeTab === tabs.tab1 && "active"}`}
								data-toggle="tab"
								href={`#${tabs.tab1}`}
								onClick={() => setActiveTab(tabs.tab1)}
							>
								ACTIVE NOW
							</a>
						</li>
						<li className="nav-item">
							<a className={`nav-link py-2 px-4 ${activeTab === tabs.tab2 && "active"}`}
								data-toggle="tab"
								href={`#${tabs.tab2}`}
								onClick={() => setActiveTab(tabs.tab2)}
							>
								PRODUCTIVITY
							</a>
						</li>
						<li className="nav-item">
							<a className={`nav-link py-2 px-4 ${activeTab === tabs.tab3 && "active"}`}
								data-toggle="tab"
								href={`#${tabs.tab3}`}
								onClick={() => setActiveTab(tabs.tab3)}
							>
								GROUPS
							</a>
						</li>
						<li className="nav-item">
							<a className={`nav-link py-2 px-4 ${activeTab === tabs.tab4 && "active"}`}
								data-toggle="tab"
								href={`#${tabs.tab4}`}
								onClick={() => setActiveTab(tabs.tab4)}
							>
								USERS
							</a>
						</li>
					</ul>
				</div>
			</div>

			{/* Body */}
			<div className="card-body">
				<div className="row">
					<TeamPluseWidgetItem className="col-xl-3 col-lg-6 col-md-6 col-sm-6 my-3" warning="success" />
					<TeamPluseWidgetItem className="col-xl-3 col-lg-6 col-md-6 col-sm-6 my-3" warning="" />
					<TeamPluseWidgetItem className="col-xl-3 col-lg-6 col-md-6 col-sm-6 my-3" warning="success" />
					<TeamPluseWidgetItem className="col-xl-3 col-lg-6 col-md-6 col-sm-6 my-3" warning="success" />
					<TeamPluseWidgetItem className="col-xl-3 col-lg-6 col-md-6 col-sm-6 my-3" warning="success" />
					<TeamPluseWidgetItem className="col-xl-3 col-lg-6 col-md-6 col-sm-6 my-3" warning="" />
					<TeamPluseWidgetItem className="col-xl-3 col-lg-6 col-md-6 col-sm-6 my-3" warning="success" />
				</div>
			</div>
		</div>
	);
}

export default TeamPluseWidget;
