import React, { Component } from "react";
import ApexCharts from 'apexcharts';

class ProductivityWidget extends Component {
	
	constructor(props) {
		super(props);
	}

	componentDidMount() {
		this._renderCharts();
	}

	_renderCharts() {
		var options = {
          series: [{
          name: 'Productive',
          data: [44]
        }, {
          name: 'Unproductive',
          data: [13]
        }, {
          name: 'Passive',
          data: [0]
        }, {
					name: 'Undefined',
					data: [0]
				}],
          chart: {
          type: 'bar',
          height: 350,
          stacked: true,
          // stackType: '100%'
        },
        responsive: [{
          breakpoint: 480,
          options: {
            legend: {
              position: 'bottom',
              offsetX: -10,
              offsetY: 0
            }
          }
        }],
        xaxis: {
          categories: ['10/27/2020'],
        },
        fill: {
          opacity: 1
        },
        legend: {
          position: 'right',
          offsetX: 0,
          offsetY: 50
        },
        };

        var chart = new ApexCharts(document.querySelector("#productivitycharts"), options);
        chart.render();
	}

	render() {
		return (
			<>
				<div className="card card-custom">
					<div class="card-header border-0 pt-5">
						<h3 class="card-title align-items-start flex-column">
							<span class="card-label font-weight-bolder text-dark">PRODUCTIVITY</span>
						</h3>
						<div class="card-toolbar">
							<ul class="nav nav-pills nav-pills-sm nav-dark-75">
								<li class="nav-item">
									<a class="nav-link py-2 px-4 active" data-toggle="tab">Day</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab">Week</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab">Month</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab">Year</a>
								</li>
							</ul>
						</div>
					</div>
					<div id="productivitycharts">
					</div>
				</div>
			</>
		)
	}
}

export default ProductivityWidget;
