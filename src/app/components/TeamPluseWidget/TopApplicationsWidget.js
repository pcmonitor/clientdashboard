import React, { Component } from "react";
import ApexCharts from 'apexcharts';

class TopApplicationsWidget extends Component {
	
	constructor(props) {
		super(props);
	}

	componentDidMount() {
		this._renderCharts();
	}

	_renderCharts() {
		var options = {
          series: [44, 55, 13, 43, 22],
          chart: {
          width: 540,
          type: 'pie',
        },
        labels: ['Figma', 'Microsoft PowerPoint', 'Windows Explorer', 'Bloco de notas', 'Spotify'],
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var chart = new ApexCharts(document.querySelector("#topapplicationschart"), options);
        chart.render();
	}

	render() {
		return (
			<>
				<div className="card card-custom">
					<div class="card-header border-0 pt-5">
						<h3 class="card-title align-items-start flex-column">
							<span class="card-label font-weight-bolder text-dark">TOP APPLICATIONS</span>
						</h3>
						<div class="card-toolbar">
							<ul class="nav nav-pills nav-pills-sm nav-dark-75">
								{/* <li class="nav-item">
									<a class="nav-link py-2 px-4 active" data-toggle="tab">Users</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab">Groups</a>
								</li> */}
							</ul>
						</div>
					</div>
					<div id="topapplicationschart">
					</div>
				</div>
			</>
		)
	}
}

export default TopApplicationsWidget;
