import React from "react";
import { toAbsoluteUrl } from "../../../_metronic/_helpers";
import SVG from "react-inlinesvg";



const TeamPluseWidgetItem = ({className, warning}) => {
	return (
		<>
			<div className={`${className}`}>
				<div className="card card-custom">
					<div class="d-flex align-items-center justify-content-between flex-grow-1" style={{padding: "1rem 1.2rem"}}>
						<div class="d-flex flex-column mr-2">
							<a class="text-dark-75 text-hover-primary font-weight-bolder font-size-h5">Erick</a>
						</div>
						{warning == "success" ? 
							<span class="symbol symbol-light-success symbol-45">
								<span class="symbol-label font-weight-bolder font-size-h6">24%</span>
							</span> : 
							<span class="symbol symbol-light-danger symbol-45">
								<span class="symbol-label font-weight-bolder font-size-h6">24%</span>
							</span>
							}
					</div>
					<div className="d-flex align-items-center flex-grow-1" style={{padding: "1rem 1.2rem"}}>
						<div className="symbol symbol-45 bg-transparent flex-shrink-0 mr-3">
            	<SVG src={toAbsoluteUrl("/media/svg/files/pdf.svg")}></SVG>
            </div>
						<div>
							<a class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">PDF</a>
							<span class="text-muted font-weight-bold d-block">Title - test.pdf</span>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}

export default TeamPluseWidgetItem;
