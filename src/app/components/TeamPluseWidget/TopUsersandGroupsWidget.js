import React, { Component } from "react";
import ApexCharts from 'apexcharts';

class TopUsersandGroupsWidget extends Component {
	
	constructor(props) {
		super(props);
	}

	componentDidMount() {
		this._renderCharts();
	}

	_renderCharts() {
		var options = {
        series: [{
          data: [44, 53, 12, 9]
        }],
        chart: {
          type: 'bar',
          height: 350,
          stacked: true,
        },
        plotOptions: {
          bar: {
            horizontal: false,
          },
        },
        stroke: {
          width: 1,
          colors: ['#fff']
        },
        title: {
          text: ''
        },
        xaxis: {
          categories: ['Productive', 'Unproductive', 'Passive','Undefined'],
          labels: {
            formatter: function (val) {
              return val
            }
          }
        },
        yaxis: {
          title: {
            text: undefined
          },
        },
        tooltip: {
          y: {
            formatter: function (val) {
              return val
            }
          }
        },
        fill: {
          opacity: 1
        },
        legend: {
          position: 'top',
          horizontalAlign: 'left',
          offsetX: 40
        }
        };

        var chart = new ApexCharts(document.querySelector("#topusersandgroupscharts"), options);
        chart.render();
	}

	render() {
		return (
			<>
				<div className="card card-custom">
					<div class="card-header border-0 pt-5">
						<h3 class="card-title align-items-start flex-column">
							<span class="card-label font-weight-bolder text-dark">TOP USERS AND GROUPS</span>
						</h3>
						<div class="card-toolbar">
							<ul class="nav nav-pills nav-pills-sm nav-dark-75">
								<li class="nav-item">
									<a class="nav-link py-2 px-4 active" data-toggle="tab">Users</a>
								</li>
								<li class="nav-item">
									<a class="nav-link py-2 px-4" data-toggle="tab">Groups</a>
								</li>
							</ul>
						</div>
					</div>
					<div id="topusersandgroupscharts">
					</div>
				</div>
			</>
		)
	}
}

export default TopUsersandGroupsWidget;
