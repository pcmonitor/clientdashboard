import React, { Component } from "react";
import { toAbsoluteUrl } from "../../../_metronic/_helpers";

export const ScreenshotsWidget = ({className}) => {
	return (
		<div className={`${className}`}>
			<div className="card card-custom gutter-b card-stretch">
				<div className="card-body px-1 py-3">
					<img src={toAbsoluteUrl("/media/bg/bg-1.jpg")} style={{height: "100px", width: "100%"}} />
				</div>
				<div className="card-footer border-0">
					<div className="font-weight-bolder"> Top Websites | Gomake </div>
					<div className="text-muted"><span><i className="far fa-check-circle"></i>   10/27/2020 06:17:34 </span></div>
					<div className="font-weight-bolder"><span><i className="fas fa-th-list"></i>   Google Chrome </span></div>
					<div className="font-weight-bolder"><span><i className="fas fa-th-list"></i>   Erick </span></div>
					<div className="font-weight-bolder"><span><i className="fas fa-th-list"></i>   Go make </span></div>
					<div className="font-weight-bolder"><span><i className="fas fa-th-list"></i>   chrome.exe </span></div>
					<div className="font-weight-bolder"><span><i className="fas fa-th-list"></i>   app.gomake.com/#/ </span></div>
				</div>
			</div>
		</div>
	)
}