import React, { Component } from "react";
import { ScreenshotsWidget } from "../components/Account/ScreenshotsWidget";

export default class Screenshots extends Component {
	render () {
		return (
			<>
				<div className="card card-custom card-stretch">
					<div className="card-header border-0 pt-6 pb-0">
						<h3 className="card-title align-items-start flex-column">
							<span className="card-label font-weight-bolder text-dark">
								Screenshots
							</span>
						</h3>
					</div>

					<div className="card-body">
						<div className="row">
							<ScreenshotsWidget className="col-xl-3 col-lg-6 col-md-6 col-sm-6"/>
							<ScreenshotsWidget className="col-xl-3 col-lg-6 col-md-6 col-sm-6"/>
							<ScreenshotsWidget className="col-xl-3 col-lg-6 col-md-6 col-sm-6"/>
							<ScreenshotsWidget className="col-xl-3 col-lg-6 col-md-6 col-sm-6"/>
							<ScreenshotsWidget className="col-xl-3 col-lg-6 col-md-6 col-sm-6"/>
							<ScreenshotsWidget className="col-xl-3 col-lg-6 col-md-6 col-sm-6"/>
							<ScreenshotsWidget className="col-xl-3 col-lg-6 col-md-6 col-sm-6"/>
						</div>
					</div>
				</div>
			</>
		)
	}
}