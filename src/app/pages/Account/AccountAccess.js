import React, { Component } from "react";

export const AccountAccess = () => {
	return (
		<>
			<div className="card card-custom card-stretch">
				<div className="card-header border-0 pt-6 pb-0">
					<h3 className="card-title align-items-start flex-column">
						<span className="card-label font-weight-bolder text-dark">
							Access
						</span>
					</h3>
					<div className="card-toolbar">
						<a className="btn btn-light font-weight-bolder">+ Add New Gomake ID</a>
					</div>
				</div>

				<div className="card-body py-0">
					<div className="datatable datatable-bordered datatable-head-custom datatable-default datatable-primary datatable-loaded" id="accesstable">
						<table className="datatable-table" style={{display: "block"}}>
							<thead className="datatable-head">
								<tr className="datatable-row" >
									<th className="datatable-cell-center datatable-cell datatable-cell-sort">Username</th>
									<th className="datatable-cell-center datatable-cell datatable-cell-sort">Role</th>
									<th className="datatable-cell-center datatable-cell datatable-cell-sort">Viewable Groups</th>
									<th className="datatable-cell-center datatable-cell datatable-cell-sort">Action</th>
								</tr>
							</thead>
							<tbody className="datatable-body">
								<tr className="datatable-row" >
									<td className="datatable-cell">test123@gmail.com</td>
									<td className="datatable-cell">Admin</td>
									<td className="datatable-cell">All Users and Computers</td>
									<td className="datatable-cell">
									</td>
								</tr>
								<tr className="datatable-row" >
									<td className="datatable-cell">test123@gmail.com</td>
									<td className="datatable-cell">Admin</td>
									<td className="datatable-cell">All Users and Computers</td>
									<td className="datatable-cell">
									</td>
								</tr>
								<tr className="datatable-row" >
									<td className="datatable-cell">test123@gmail.com</td>
									<td className="datatable-cell">Admin</td>
									<td className="datatable-cell">All Users and Computers</td>
									<td className="datatable-cell">
									</td>
								</tr>
								<tr className="datatable-row" >
									<td className="datatable-cell">test123@gmail.com</td>
									<td className="datatable-cell">Admin</td>
									<td className="datatable-cell">All Users and Computers</td>
									<td className="datatable-cell">
									</td>
								</tr>
								<tr className="datatable-row" >
									<td className="datatable-cell">test123@gmail.com</td>
									<td className="datatable-cell">Admin</td>
									<td className="datatable-cell">All Users and Computers</td>
									<td className="datatable-cell">
									</td>
								</tr>
								<tr className="datatable-row" >
									<td className="datatable-cell">test123@gmail.com</td>
									<td className="datatable-cell">Admin</td>
									<td className="datatable-cell">All Users and Computers</td>
									<td className="datatable-cell">
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</>
	)
}