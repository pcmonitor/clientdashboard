import React, { Component } from "react";

export const SecurityAudit = () => {
	return (
		<>
			<div className="card card-custom card-stretch">
				<div className="card-header border-0 pt-6 pb-0">
					<h3 className="card-title align-items-start flex-column">
						<span className="card-label font-weight-bolder text-dark">
							Security Audit
						</span>
					</h3>
					<div className="card-toolbar">
						<a className="btn btn-light font-weight-bolder mr-3"> Refresh </a>
						<a className="btn btn-light font-weight-bolder"> Create Alarm </a>
					</div>
				</div>

				<div className="card-body py-0">
					<div className="datatable datatable-bordered datatable-head-custom datatable-default datatable-primary datatable-loaded" id="accesstable">
						<table className="datatable-table" style={{display: "block"}}>
							<thead className="datatable-head">
								<tr className="datatable-row" style={{left: "0px"}}>
									<th className="datatable-cell-center datatable-cell" style={{width: "50px"}}>Date/Time</th>
									<th className="datatable-cell-center datatable-cell" style={{minWidth: "120px"}}>Gomake ID</th>
									<th className="datatable-cell-center datatable-cell" style={{minWidth: "100px"}}>Public IP</th>
									<th className="datatable-cell-center datatable-cell" style={{minWidth: "100px"}}>Event</th>
									<th className="datatable-cell-center datatable-cell" style={{minWidth: "150px"}}>Description</th>
									<th className="datatable-cell-center datatable-cell" style={{minWidth: "80px"}}>Action Type</th>
									<th className="datatable-cell-center datatable-cell" style={{minWidth: "120px"}}>Action Data</th>
									<th className="datatable-cell-center datatable-cell" style={{minWidth: "50px"}}>Status</th>
								</tr>
							</thead>
							<tbody className="datatable-body">
								<tr className="datatable-row">
									<td className="datatable-cell-center datatable-cell">12/11/20 04:50:35a</td>
									<td className="datatable-cell-center datatable-cell">test@gmail.com</td>
									<td className="datatable-cell-center datatable-cell">100.10.22.30</td>
									<td className="datatable-cell-center datatable-cell">UserLogin</td>
									<td className="datatable-cell-center datatable-cell">User Logged In</td>
									<td className="datatable-cell-center datatable-cell">Update</td>
									<td className="datatable-cell-center datatable-cell">&nbsp;</td>
									<td className="datatable-cell-center datatable-cell"><i class="fa fa-check" aria-hidden="true"></i></td>
								</tr>
								<tr className="datatable-row">
									<td className="datatable-cell-center datatable-cell">12/11/20 04:50:35a</td>
									<td className="datatable-cell-center datatable-cell">test@gmail.com</td>
									<td className="datatable-cell-center datatable-cell">100.10.22.30</td>
									<td className="datatable-cell-center datatable-cell">UserLogin</td>
									<td className="datatable-cell-center datatable-cell">User Logged In</td>
									<td className="datatable-cell-center datatable-cell">Update</td>
									<td className="datatable-cell-center datatable-cell">&nbsp;</td>
									<td className="datatable-cell-center datatable-cell"><i class="fa fa-check" aria-hidden="true"></i></td>
								</tr>
								<tr className="datatable-row">
									<td className="datatable-cell-center datatable-cell">12/11/20 04:50:35a</td>
									<td className="datatable-cell-center datatable-cell">test@gmail.com</td>
									<td className="datatable-cell-center datatable-cell">100.10.22.30</td>
									<td className="datatable-cell-center datatable-cell">UserLogin</td>
									<td className="datatable-cell-center datatable-cell">User Logged In</td>
									<td className="datatable-cell-center datatable-cell">Update</td>
									<td className="datatable-cell-center datatable-cell">&nbsp;</td>
									<td className="datatable-cell-center datatable-cell"><i class="fa fa-check" aria-hidden="true"></i></td>
								</tr>
								<tr className="datatable-row">
									<td className="datatable-cell-center datatable-cell">12/11/20 04:50:35a</td>
									<td className="datatable-cell-center datatable-cell">test@gmail.com</td>
									<td className="datatable-cell-center datatable-cell">100.10.22.30</td>
									<td className="datatable-cell-center datatable-cell">UserLogin</td>
									<td className="datatable-cell-center datatable-cell">User Logged In</td>
									<td className="datatable-cell-center datatable-cell">Update</td>
									<td className="datatable-cell-center datatable-cell">&nbsp;</td>
									<td className="datatable-cell-center datatable-cell"><i class="fa fa-check" aria-hidden="true"></i></td>
								</tr>
								<tr className="datatable-row">
									<td className="datatable-cell-center datatable-cell">12/11/20 04:50:35a</td>
									<td className="datatable-cell-center datatable-cell">test@gmail.com</td>
									<td className="datatable-cell-center datatable-cell">100.10.22.30</td>
									<td className="datatable-cell-center datatable-cell">UserLogin</td>
									<td className="datatable-cell-center datatable-cell">User Logged In</td>
									<td className="datatable-cell-center datatable-cell">Update</td>
									<td className="datatable-cell-center datatable-cell">&nbsp;</td>
									<td className="datatable-cell-center datatable-cell"><i class="fa fa-check" aria-hidden="true"></i></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</>
	)
}
