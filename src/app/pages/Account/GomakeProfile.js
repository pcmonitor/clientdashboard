import React, { Component } from "react";

export const GomakeProfile = () => {
	return (
		<>
			<div className="card card-custom">
				<div className="card-header border-0 py-5">
					<h3 className="card-title align-items-start flex-column">
						<span className="card-label font-weight-bolder text-dark">
							Gomake Profile
						</span>
						<span className="text-muted mt-3 font-weight-bold font-size-sm">
							Profile Settings
						</span>
					</h3>
				</div>
				<form className="class">
					<div className="card-body">
						<div className="row">
							<div className="col-lg-6">
								<div className="form-group">
									<label>Full Name:</label>
									<input type="text" className="form-control" placeholder="Enter First name" />
								</div>
							</div>
							<div className="col-lg-6">
								<div className="form-group">
									<label>Last Name:</label>
									<input type="text" className="form-control" placeholder="Enter Last name" />
								</div>
							</div>
						</div>

						<div className="row">
							<div className="col-lg-6">
								<div className="form-group">
									<label>Phone number:</label>
									<input type="text" className="form-control" placeholder="Enter Phone number" />
								</div>
							</div>
							<div className="col-lg-6">
								<div className="form-group">
									<label>ROLE:</label>
									<select className="form-control form-control-solid">
										<option value="1">Analyst</option>
										<option value="2">Architect</option>
										<option value="3">C-Level</option>
										<option value="4">CEO/President</option>
										<option value="5">Chancellor/Dean/Provost</option>
										<option value="6">Consultant/System Integrator</option>
										<option value="7">Coordinator/Sepcialist</option>
										<option value="8">Developer/Enginner</option>
										<option value="9">Director</option>
										<option value="10">Manager</option>
									</select>
								</div>
							</div>
						</div>

						<div className="row">
							<div className="col-lg-12">
								<div className="form-group">
									<label>Department:</label>
									<select className="form-control form-control-solid">
										<option value="1">Administration(Management)</option>
										<option value="1">Analyst</option>
										<option value="1">Channel Partner</option>
										<option value="1">Engineering/R&D</option>
										<option value="1">Finance</option>
										<option value="1">Human Resources</option>
										<option value="1">IT</option>
										<option value="1">Legal</option>
										<option value="1">Manufacturing</option>
										<option value="1">Marketing</option>
										<option value="1">Product Management</option>
										<option value="1">Purchasing</option>
										<option value="1">Sales</option>
										<option value="1">Supply Chain & Distribution</option>
										<option value="1">Support/Service</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</form>

				<div className="card-footer">
					<div className="row">
						<div className="col-2">
							<div className="form-group">
								<button type="reset" className="form-control btn btn-success">Update Profile</button>
							</div>
						</div>
						<div className="col-8" />
						<div className="col-2">
							<div className="form-group">
								<button type="reset" className="form-control btn btn-secondary">Cancel Changes</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}