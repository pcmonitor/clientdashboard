import React, { Component } from "react";
import { Dropdown, Label, Button, Accordion } from "react-bootstrap";

export const ChromebookUsers = () => {
	return (
		<>
			<div className="card card-custom card-stretch">
				<div className="card-header border-0 pt-6 pb-0">
					<h3 className="card-title align-items-start flex-column">
						<span className="card-label font-weight-bolder text-dark">
							Chromebook Users
						</span>
					</h3>
					<div className="card-toolbar">
						<a className="btn btn-success font-weight-bolder mr-3"> ADD </a>
						<a className="btn btn-danger font-weight-bolder"> REMOVE </a>
					</div>
				</div>

				<div className="card-body py-0">
					<div className="datatable datatable-bordered datatable-head-custom datatable-default datatable-primary datatable-loaded" id="accesstable">
						<table className="datatable-table" style={{display: "block"}}>
							<thead className="datatable-head">
								<tr className="datatable-row" style={{left: "0px"}}>
									<th data-field="check" className="datatable-cell-center datatable-cell datatable-cell-sort" style={{width: "50px"}}>Select</th>
									<th data-field="googleid" className="datatable-cell-center datatable-cell datatable-cell-sort" style={{minWidth: "120px"}}>Google ID</th>
									<th data-field="status" className="datatable-cell-center datatable-cell datatable-cell-sort" style={{minWidth: "150px"}}>Status</th>
									<th data-field="Action" className="datatable-cell-center datatable-cell datatable-cell-sort" style={{minWidth: "100px"}}>Action</th>
								</tr>
							</thead>
							<tbody className="datatable-body">
								<tr className="datatable-row" style={{left: "0px"}}>
									<td className="datatable-cell-center datatable-cell datatable-cell-sort" style={{width: "50px"}}>
										<label class="checkbox checkbox-outline checkbox-success">
											<input type="checkbox" name="Checkboxes15"/>
											<span></span>
										</label>
									</td>
									<td className="datatable-cell-center datatable-cell datatable-cell-sort" style={{minWidth: "120px"}}>test@gmail.com</td>
									<td className="datatable-cell-center datatable-cell datatable-cell-sort" style={{minWidth: "150px"}}>Invite sent</td>
									<td className="datatable-cell-right datatable-cell datatable-cell-sort" style={{minWidth: "100px"}}>
										<Button variant="btn btn-secondary" className="mx-2">Resend</Button>
                                		<Button variant="btn btn-secondary" className="mx-2">Cancel</Button>
									</td>
								</tr>
								<tr className="datatable-row" style={{left: "0px"}}>
									<td className="datatable-cell-center datatable-cell datatable-cell-sort" style={{width: "50px"}}>
										<label class="checkbox checkbox-outline checkbox-success">
											<input type="checkbox" name="Checkboxes15"/>
											<span></span>
										</label>
									</td>
									<td className="datatable-cell-center datatable-cell datatable-cell-sort" style={{minWidth: "120px"}}>test@gmail.com</td>
									<td className="datatable-cell-center datatable-cell datatable-cell-sort" style={{minWidth: "150px"}}>Invite sent</td>
									<td className="datatable-cell-right datatable-cell datatable-cell-sort" style={{minWidth: "100px"}}>
										<Button variant="btn btn-secondary" className="mx-2">Resend</Button>
                                		<Button variant="btn btn-secondary" className="mx-2">Cancel</Button>
									</td>
								</tr>
								<tr className="datatable-row" style={{left: "0px"}}>
									<td className="datatable-cell-center datatable-cell datatable-cell-sort" style={{width: "50px"}}>
										<label class="checkbox checkbox-outline checkbox-success">
											<input type="checkbox" name="Checkboxes15"/>
											<span></span>
										</label>
									</td>
									<td className="datatable-cell-center datatable-cell datatable-cell-sort" style={{minWidth: "120px"}}>test@gmail.com</td>
									<td className="datatable-cell-center datatable-cell datatable-cell-sort" style={{minWidth: "150px"}}>Invite sent</td>
									<td className="datatable-cell-right datatable-cell datatable-cell-sort" style={{minWidth: "100px"}}>
										<Button variant="btn btn-secondary" className="mx-2">Resend</Button>
                                		<Button variant="btn btn-secondary" className="mx-2">Cancel</Button>
									</td>
								</tr>
								<tr className="datatable-row" style={{left: "0px"}}>
									<td className="datatable-cell-center datatable-cell datatable-cell-sort" style={{width: "50px"}}>
										<label class="checkbox checkbox-outline checkbox-success">
											<input type="checkbox" name="Checkboxes15"/>
											<span></span>
										</label>
									</td>
									<td className="datatable-cell-center datatable-cell datatable-cell-sort" style={{minWidth: "120px"}}>test@gmail.com</td>
									<td className="datatable-cell-center datatable-cell datatable-cell-sort" style={{minWidth: "150px"}}>Invite sent</td>
									<td className="datatable-cell-right datatable-cell datatable-cell-sort" style={{minWidth: "100px"}}>
										<Button variant="btn btn-secondary" className="mx-2">Resend</Button>
                                		<Button variant="btn btn-secondary" className="mx-2">Cancel</Button>
									</td>
								</tr>
								<tr className="datatable-row" style={{left: "0px"}}>
									<td className="datatable-cell-center datatable-cell datatable-cell-sort" style={{width: "50px"}}>
										<label class="checkbox checkbox-outline checkbox-success">
											<input type="checkbox" name="Checkboxes15"/>
											<span></span>
										</label>
									</td>
									<td className="datatable-cell-center datatable-cell datatable-cell-sort" style={{minWidth: "120px"}}>test@gmail.com</td>
									<td className="datatable-cell-center datatable-cell datatable-cell-sort" style={{minWidth: "150px"}}>Invite sent</td>
									<td className="datatable-cell-right datatable-cell datatable-cell-sort" style={{minWidth: "100px"}}>
										<Button variant="btn btn-secondary" className="mx-2">Resend</Button>
                                		<Button variant="btn btn-secondary" className="mx-2">Cancel</Button>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</>
	)
}
