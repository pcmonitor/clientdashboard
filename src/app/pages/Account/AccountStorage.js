import React, { Component } from "react";

export const AccountStorage = () => {
	return (
		<>
			<div className="card card-custom card-stretch">
				<div className="card-header border-0 pt-6 pb-0">
					<h3 className="card-title align-items-start flex-column">
						<span className="card-label font-weight-bolder text-dark">
							Storage
						</span>
					</h3>
					<div className="card-toolbar">
						<a className="btn btn-success font-weight-bolder mr-3"> Export </a>
						<a className="btn btn-danger font-weight-bolder"> Delete Selected </a>
					</div>
				</div>

				<div className="card-body py-0">
					<div className="datatable datatable-bordered datatable-head-custom datatable-default datatable-primary datatable-loaded" id="accesstable">
						<table className="datatable-table" style={{display: "block"}}>
							<thead className="datatable-head">
								<tr className="datatable-row" style={{left: "0px"}}>
									<th className="datatable-cell-center datatable-cell" style={{width: "100px"}}>Primary Domain</th>
									<th className="datatable-cell-center datatable-cell" style={{minWidth: "100px"}}>Company</th>
									<th className="datatable-cell-center datatable-cell" style={{minWidth: "90px"}}>Company Alias</th>
									<th className="datatable-cell-center datatable-cell" style={{minWidth: "60px"}}>Agent Version</th>
									<th className="datatable-cell-center datatable-cell" style={{minWidth: "30px"}}>OS</th>
									<th className="datatable-cell-center datatable-cell" style={{minWidth: "70px"}}>Last Log Record</th>
									<th className="datatable-cell-center datatable-cell" style={{minWidth: "70px"}}>Last Screenshot</th>
									<th className="datatable-cell-center datatable-cell" style={{minWidth: "50px"}}># of Log Records</th>
									<th className="datatable-cell-center datatable-cell" style={{minWidth: "50px"}}># of Log Screenshots</th>
									<th className="datatable-cell-center datatable-cell" style={{minWidth: "80px"}}>Total Storage(GB)</th>
								</tr>
							</thead>
							<tbody className="datatable-body">
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</>
	)
}
