import React, { Component } from "react";

export const AccountNotifications = () => {
	return (
		<>
			<div className="card card-custom card-stretch">
				<div className="card-header border-0 pt-6 pb-0">
					<h3 className="card-title align-items-start flex-column">
						<span className="card-label font-weight-bolder text-dark">
							Security Audit
						</span>
					</h3>
					<div className="card-toolbar">
						<a className="btn btn-light font-weight-bolder"> Refresh </a>
					</div>
				</div>

				<div className="card-body py-0">
					<div className="datatable datatable-bordered datatable-head-custom datatable-default datatable-primary datatable-loaded" id="accesstable">
						<table className="datatable-table" style={{display: "block"}}>
							<thead className="datatable-head">
								<tr className="datatable-row" style={{left: "0px"}}>
									<th className="datatable-cell-center datatable-cell" style={{minWidth: "80px"}}>Date/Time</th>
									<th className="datatable-cell-center datatable-cell" style={{minWidth: "150px"}}>Alarm</th>
									<th className="datatable-cell-center datatable-cell" style={{minWidth: "80px"}}>Attempts</th>
									<th className="datatable-cell-center datatable-cell" style={{minWidth: "80px"}}>State</th>
									<th className="datatable-cell-center datatable-cell" style={{minWidth: "150px"}}>Payload</th>
								</tr>
							</thead>
							<tbody className="datatable-body">
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</>
	)
}
