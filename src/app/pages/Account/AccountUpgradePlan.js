import React, { Component } from "react";

export const AccountUpgradePlan = () => {
	return (
		<>
			<div className="card card-custom card-stretch">
				<div className="card-header border-0 pt-6 pb-0">
					<h3 className="card-title align-items-start flex-column">
						<span className="card-label font-weight-bolder text-dark">
							Upgrade Plans
						</span>
					</h3>
				</div>

				<div className="card-body py-0">
					Coming soon!
				</div>
			</div>
    </>
  )
}
