import { Card } from "@material-ui/core";
import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import { Dropdown, Label, Button, Accordion } from "react-bootstrap";
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
}));

export const Blocking = () => {

  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleChange = panel => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <div>
      <div className="card card-custom card-stretch">
        <div className="card-header border-0 pt-6 pb-0">
            <div className="card-header px-0 border-bottom-0">
                <p><i className="fa fa-exclamation-triangle text-warning" aria-hidden="true"></i>Because of DNS caching on the client by the OS or the browser it may take a few minutes for the changes to take effect</p>
            </div>
        </div>

        <div className="card-body py-0">
            <div className="container px-0">   
                <div className="row"  >
                    <div className="col-6">
                        <Button variant="btn btn-outline-secondary" className="mx-2">Groups</Button>
                        <Button variant="btn btn-outline-secondary" className="mx-2">Domains</Button>
                    </div>
                    <div className="col-6 text-right">
                        <Button variant="btn btn-success" className="mx-2">Apply</Button>
                        <Button variant="btn btn-danger" className="mx-2">Cancel</Button>
                    </div>
                </div>
            </div>
            <div className="row mt-5">
                <div className="col-6">
                    <div className="card card-custom gutter-b" style={{ "border": "1px solid #ddd"}}>
                        <div className="card-header bg-secondary">
                            <div className="card-title">
                                <h3 className="card-label">Group</h3>
                            </div>
                            <div className="card-toolbar">
                                <Button variant="btn btn-success" className="mx-2">Create Group</Button>
                            </div>
                        </div>
                        <div className="card-body pt-0 px-0">
                            <input type="text" className="form-control" placeholder="Filter Groups..."/>
                            <table className="table table-hover">
                                <tbody>
                                    <tr className="table-dark">
                                        <td><i className="flaticon-laptop mr-2"></i>All Computers ( 0 group members )</td>
                                    </tr>
                                    <tr>
                                        <td><i className="flaticon-laptop mr-2"></i>All Computers ( 0 group members )</td>
                                    </tr>
                                    <tr>
                                        <td><i className="flaticon-laptop mr-2"></i>All Computers ( 0 group members )</td>
                                    </tr>
                                    <tr>
                                        <td><i className="flaticon-laptop mr-2"></i>All Computers ( 0 group members )</td>
                                    </tr>
                                    <tr>
                                        <td><i className="flaticon-laptop mr-2"></i>All Computers ( 0 group members )</td>
                                    </tr>
                                    <tr>
                                        <td><i className="flaticon-laptop mr-2"></i>All Computers ( 0 group members )</td>
                                    </tr>
                                    <tr>
                                        <td><i className="flaticon-laptop mr-2"></i>All Computers ( 0 group members )</td>
                                    </tr>
                                    <tr>
                                        <td><i className="flaticon-laptop mr-2"></i>All Computers ( 0 group members )</td>
                                    </tr>
                                    <tr>
                                        <td><i className="flaticon-laptop mr-2"></i>All Computers ( 0 group members )</td>
                                    </tr>
                                    <tr>
                                        <td><i className="flaticon-laptop mr-2"></i>All Computers ( 0 group members )</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div className="col-6">
                    <div className="card card-custom gutter-b" style={{ "border": "1px solid #ddd"}}>
                        <div className="card-header bg-secondary">
                            <div className="card-title">
                                <h3 className="card-label">Blocked Domains (All Computers)</h3>
                            </div>
                            <div className="card-toolbar">
                                <Button variant="btn btn-success" className="mx-2">Add Domains</Button>
                                <Button variant="btn btn-danger" className="mx-2">Remove All</Button>
                            </div>
                        </div>
                        <div className="card-body pt-0 px-0">
                            <input type="text" className="form-control" placeholder="Filter Domains..."/>
                            <table className="table table-hover">
                                <tbody>
                                    <tr className="table-dark">
                                        <td><i class="flaticon-globe mr-2"></i>www.google.com</td>
                                    </tr>
                                    <tr>
                                        <td><i class="flaticon-globe mr-2"></i>www.baidu.com</td>
                                    </tr>
                                    <tr>
                                        <td><i class="flaticon-globe mr-2"></i>www.domain.com</td>
                                    </tr>
                                    <tr>
                                        <td><i class="flaticon-globe mr-2"></i>www.domain.com</td>
                                    </tr>
                                    <tr>
                                        <td><i class="flaticon-globe mr-2"></i>www.domain.com</td>
                                    </tr>
                                    <tr>
                                        <td><i class="flaticon-globe mr-2"></i>www.domain.com</td>
                                    </tr>
                                    <tr>
                                        <td><i class="flaticon-globe mr-2"></i>www.domain.com</td>
                                    </tr>
                                    <tr>
                                        <td><i class="flaticon-globe mr-2"></i>www.domain.com</td>
                                    </tr>
                                    <tr>
                                        <td><i class="flaticon-globe mr-2"></i>www.domain.com</td>
                                    </tr>
                                    <tr>
                                        <td><i class="flaticon-globe mr-2"></i>www.domain.com</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div className="card-footer py-0">
        </div>
      </div>
    </div>
  );
}

