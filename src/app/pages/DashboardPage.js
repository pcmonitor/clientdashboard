import React from "react";
import TeamPluseWidget from "../components/TeamPluseWidget/TeamPluseWidget";
import ProductivityWidget from "../components/TeamPluseWidget/ProductivityWidget";
import TopUsersandGroupsWidget from "../components/TeamPluseWidget/TopUsersandGroupsWidget";
import TopApplicationsWidget from "../components/TeamPluseWidget/TopApplicationsWidget";
import TopSitesWidget from "../components/TeamPluseWidget/TopSitesWidget";

export function DashboardPage() {
  return (
    <>
      <div className="row">
        <div className="col-lg-12 col-xxl-12 px-1 py-0">
          <TeamPluseWidget />
        </div>
      </div>

      <div className="row">
        <div className="col-lg-6 col-xxl-6 col-sm-6 px-1 py-1">
          <ProductivityWidget />
        </div>
        <div className="col-lg-6 col-xxl-6 col-sm-6 px-1 py-1">
          <TopUsersandGroupsWidget />
        </div>
      </div>

      <div className="row">
        <div className="col-lg-6 col-xxl-6 col-sm-6 px-1 py-1">
          <TopApplicationsWidget />
        </div>
        <div className="col-lg-6 col-xxl-6 col-sm-6 px-1 py-1">
          <TopSitesWidget />
        </div>
      </div>
    </>
  )
}
