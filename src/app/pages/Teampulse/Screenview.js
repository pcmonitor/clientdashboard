import { Card } from "@material-ui/core";
import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import { Dropdown, Label, Button, Accordion, Tabs, Tab } from "react-bootstrap";
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { toAbsoluteUrl } from "../../../_metronic/_helpers";

export const Screenview = () => {
    return (
        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-6 my-3">    
            <div className="card card-custom card-stretch" style={{ boxShadow: "1px 1px 3px 3px #ddd" }}>
                <div className="card-body">        
                    <div className="row">
                        <img src={toAbsoluteUrl("/media/bg/bg-1.jpg")} style={{height: "200px", width: "100%"}} />
                    </div>
                    <div className="text-success font-weight-bold py-3">
                        Passive
                    </div>
                    <div className="total-time py-3 border-bottom">
                        <i className="flaticon-laptop mr-3"></i>DESKTOP-001
                    </div>
                    <div className="category border-bottom py-3">
                        <i className="far fa-user mr-3"></i>Jhon
                    </div>
                    <div className="category pt-3">
                        <i className="far fa-sun mr-3"></i>Windows Explorer
                    </div>
                </div>
            </div>
        </div>
    );
}
