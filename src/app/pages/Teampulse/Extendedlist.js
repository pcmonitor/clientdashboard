import { Card } from "@material-ui/core";
import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import { Dropdown, Label, Button, Accordion, Tabs, Tab, Table } from "react-bootstrap";
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

export const Extendedlist = () => {
    return (
        <div className="my-5">
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th><i className="flaticon-laptop"></i></th>
                        <th>OS</th>
                        <th>Computer</th>
                        <th>Computer Alias</th>
                        <th>User</th>
                        <th>Title</th>
                        <th>Executable</th>
                        <th>URL</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><input type="text" className="form-control"/></td>
                        <td><input type="text" className="form-control"/></td>
                        <td><input type="text" className="form-control"/></td>
                        <td className="text-muted"><input type="text" className="form-control"/></td>
                        <td><input type="text" className="form-control"/></td>
                        <td><input type="text" className="form-control"/></td>
                    </tr>
                    <tr>
                        <td><i className="flaticon-laptop text-success"></i></td>
                        <td><i className="fab fa-windows"></i></td>
                        <td>DESKTOP-001</td>
                        <td>DESKTOP-001</td>
                        <td>John</td>
                        <td className="text-primary">Explorer</td>
                        <td>chrome.exe</td>
                        <td>google.com/?search=GoMaker</td>
                    </tr>
                    <tr>
                        <td><i className="flaticon-laptop text-success"></i></td>
                        <td><i className="fab fa-windows"></i></td>
                        <td>DESKTOP-002</td>
                        <td>DESKTOP-002</td>
                        <td>John</td>
                        <td className="text-primary">Desktop</td>
                        <td>chrome.exe</td>
                        <td>google.com/?search=GoMaker</td>
                    </tr>
                    <tr>
                        <td><i className="flaticon-laptop text-success"></i></td>
                        <td><i className="fab fa-windows"></i></td>
                        <td>DESKTOP-003</td>
                        <td>DESKTOP-003</td>
                        <td>John</td>
                        <td className="text-primary">Adobe PhotoShop</td>
                        <td>chrome.exe</td>
                        <td>google.com/?search=GoMaker</td>
                    </tr>
                    <tr>
                        <td><i className="flaticon-laptop text-success"></i></td>
                        <td><i className="fab fa-windows"></i></td>
                        <td>DESKTOP-004</td>
                        <td>DESKTOP-004</td>
                        <td>John</td>
                        <td className="text-primary">Visual Studio</td>
                        <td>chrome.exe</td>
                        <td>google.com/?search=GoMaker</td>
                    </tr>
                    <tr>
                        <td><i className="flaticon-laptop text-success"></i></td>
                        <td><i className="fab fa-windows"></i></td>
                        <td>DESKTOP-004</td>
                        <td>DESKTOP-004</td>
                        <td>John</td>
                        <td className="text-primary">Explorer</td>
                        <td>chrome.exe</td>
                        <td>google.com/?search=GoMaker</td>
                    </tr>
                    <tr>
                        <td><i className="flaticon-laptop text-success"></i></td>
                        <td><i className="fab fa-windows"></i></td>
                        <td>DESKTOP-004</td>
                        <td>DESKTOP-004</td>
                        <td>John</td>
                        <td className="text-primary">Process Manager</td>
                        <td>chrome.exe</td>
                        <td>google.com/?search=GoMaker</td>
                    </tr>
                    <tr>
                        <td><i className="flaticon-laptop text-success"></i></td>
                        <td><i className="fab fa-windows"></i></td>
                        <td>DESKTOP-004</td>
                        <td>DESKTOP-004</td>
                        <td>John</td>
                        <td className="text-primary">Skype</td>
                        <td>chrome.exe</td>
                        <td>google.com/?search=GoMaker</td>
                    </tr>
                </tbody>
            </Table>
        </div>
    );
}
