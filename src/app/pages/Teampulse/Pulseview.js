import { Card } from "@material-ui/core";
import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import { Dropdown, Label, Button, Accordion, Tabs, Tab, ProgressBar, Table } from "react-bootstrap";
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

export const Pulseview = () => {

    const now = 60;
    const progressInstance = <ProgressBar now={now} label={`${now}%`} />;

    return (
        <div className="col-xl-3 col-lg-6 col-md-6 col-sm-6 my-3">
            <div className="card card-custom card-stretch" style={{ boxShadow: "1px 1px 3px 3px #ddd" }}>
                <div className="card-header">
                    <div className="card-title">
                        <i className="flaticon-laptop mr-3"></i><h5 className="card-label">DESKTOP-0001</h5>
                    </div>
                    <div className="card-toolbar">
                        <i className="flaticon2-user"></i>
                    </div>
                </div>
                <div className="card-body">
                    <div className="row">
                        <p>google.com/search?q=gomaker</p>
                    </div>
                    <div className="row" style={{ position:"relative"}}>
                        <div className="py-2" style={{position:"absolute", margin:"auto", width:"100%", textAlign:"center"}}>
                            <h2>75%</h2>
                            <h5 style={{fontSize: 14}}>Productive</h5>
                        </div>
                        <div className="progress" style={{ width:"100%", height:60}}>
                            <div className="progress-bar" role="progressbar" style={{ width: "75%"}} aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div className="total-time py-3 text-center border-bottom">
                        6 Hours Total Time
                    </div>
                    <div className="category border-bottom py-3">
                        <p className="text-muted">No Category</p>
                        <p>Top Category</p>
                    </div>
                    <div className="category pt-3">
                        <p className="text-muted">No Activity</p>
                        <p>Top Activity</p>
                    </div>
                </div>
            </div>
        </div>
    );
}
