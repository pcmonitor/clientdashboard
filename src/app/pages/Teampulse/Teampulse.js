import { Card } from "@material-ui/core";
import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import { Dropdown, Label, Button, Accordion, Tabs, Tab } from "react-bootstrap";
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Pulseview } from './Pulseview';
import { Extendedlist } from './Extendedlist';
import { Screenview } from './Screenview';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
}));

export const Teampulse = () => {

  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleChange = panel => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <div>
      <div className="card card-custom card-stretch">
        <div className="card-header border-0 pt-6 pb-0">
          <div className="card-toolbar">

            <Dropdown className="mx-2">
              <Dropdown.Toggle variant="btn btn-outline-secondary" id="dropdown-datetype">
                <i className="flaticon2-group"></i>All Users
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item >All Users</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Button variant="btn btn-outline-secondary" className="mx-2"><i className="flaticon-refresh"></i>Refresh</Button>
          </div>
        </div>

        <div className="card-body py-0">
            <div className="d-flex float-right">
                <Dropdown className="mx-2">
                    <Dropdown.Toggle variant="btn btn-outline-secondary" id="dropdown-datetype">
                        <i className="fas fa-sort-amount-down"></i>ACTIVE NOW
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                        <Dropdown.Item >All</Dropdown.Item>
                        <Dropdown.Item >Active Now</Dropdown.Item>
                        <Dropdown.Item >Active Today</Dropdown.Item>
                        <Dropdown.Item >Passive Now</Dropdown.Item>
                        <Dropdown.Item >Inactive Now</Dropdown.Item>
                        <Dropdown.Item >Inactive Today</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>

                <Dropdown className="mx-2">
                    <Dropdown.Toggle variant="btn btn-outline-secondary" id="dropdown-datetype">
                        <i className="fas fa-sort-amount-down-alt" aria-hidden="true"></i>PRODUCTIVITY %
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                        <Dropdown.Item >Productivity %</Dropdown.Item>
                        <Dropdown.Item >productive Time</Dropdown.Item>
                        <Dropdown.Item >Total Time</Dropdown.Item>
                        <Dropdown.Item >Username</Dropdown.Item>
                        <Dropdown.Item >First Seen</Dropdown.Item>
                        <Dropdown.Item >Last Seen</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
                <Button variant="btn btn-dark" className="mx-2">USERS</Button>
                <Button variant="btn btn-outline-secondary" className="mx-2">GROUPS</Button>
                <Button variant="btn btn-outline-secondary" className="mx-2"><i className="fas fa-pause"></i></Button>
                <Button variant="btn btn-outline-secondary" className="mx-2"><i className="fa fa-cog" aria-hidden="true"></i></Button>
            </div>
            
            <Tabs defaultActiveKey="pulse-view" id="uncontrolled-tab-example">
                <Tab eventKey="pulse-view" title="PULSE VIEW">
                  <div className="row my-5">
                    <Pulseview/>
                    <Pulseview/>
                    <Pulseview/>
                    <Pulseview/>
                    <Pulseview/>
                    <Pulseview/>
                    <Pulseview/>
                    <Pulseview/>
                  </div>
                </Tab>
                <Tab eventKey="extended-list" title="EXTENDED LIST">
                    <Extendedlist/>
                </Tab>
                <Tab eventKey="screen-view" title="SCREEN VIEW">
                  <div className="row my-5">
                    <Screenview/>
                    <Screenview/>
                    <Screenview/>
                    <Screenview/>
                    <Screenview/>
                    <Screenview/>
                    <Screenview/>
                  </div>
                </Tab>
            </Tabs>
        </div>

        <div className="card-footer py-0">
        </div>
      </div>
    </div>
  );
}

