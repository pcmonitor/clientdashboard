import { Card } from "@material-ui/core";
import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import { Dropdown, Label, Button, Accordion, Table } from "react-bootstrap";

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
}));

export const WorkingHours = () => {

  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleChange = panel => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <div>
      <div className="card card-custom card-stretch">
        <div className="card-header border-0 pt-6 pb-0">
          <div className="card-toolbar">
            <Dropdown className="mx-2">
              <Dropdown.Toggle variant="btn btn-outline-secondary" id="dropdown-datetype">
                Today
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item >Today</Dropdown.Item>
                <Dropdown.Item >Yesterday</Dropdown.Item>
                <Dropdown.Item >Last 7 Days</Dropdown.Item>
                <Dropdown.Item >Last 30 days</Dropdown.Item>
                <Dropdown.Divider/>
                <Dropdown.Item >Custom Range</Dropdown.Item>
                <Dropdown.Divider/>
                <Dropdown.Item >This Week</Dropdown.Item>
                <Dropdown.Item >Last Week</Dropdown.Item>
                <Dropdown.Divider/>
                <Dropdown.Item >This Month</Dropdown.Item>
                <Dropdown.Item >Last Month</Dropdown.Item>
                <Dropdown.Item >This Year</Dropdown.Item>
                <Dropdown.Item >Last Year</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>

            <Dropdown className="mx-2">
              <Dropdown.Toggle variant="btn btn-outline-secondary" id="dropdown-datetype">
                <i className="flaticon2-group"></i>All Users
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item >All Users</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Button variant="btn btn-outline-secondary" className="mx-2"><i className="flaticon-refresh"></i>Refresh</Button>
            <Dropdown className="mx-2">
              <Dropdown.Toggle variant="btn btn-outline-secondary" id="dropdown-datetype">
                Column
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item >Select Columns</Dropdown.Item>
                <Dropdown.Item >Restore Columns</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown className="mx-2">
              <Dropdown.Toggle variant="btn btn-outline-secondary" id="dropdown-datetype">
                <i className="flaticon-attachment"></i>Export
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item >Websites only</Dropdown.Item>
                <Dropdown.Item >Websites and Titles</Dropdown.Item>
                <Dropdown.Item >Websites and Subpages</Dropdown.Item>
                <Dropdown.Item >Websites and Users</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </div>
        </div>

        <div className="card-body py-0 my-5">
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Date</th>
                <th>Groups</th>
                <th>Computer</th>
                <th>User</th>
                <th>First Activity</th>
                <th>Last Activity</th>
                <th>Last Activity Log</th>
                <th>Total Time</th>
                <th>Active Time</th>
                <th>Productive</th>
                <th>Unproductive</th>
                <th>Undefined</th>
                <th>Passive Time</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>01/01/2020</td>
                <td>K.S</td>
                <td>Windows</td>
                <td>DESKTOP-001</td>
                <td>13:00:23</td>
                <td>15:20:23</td>
                <td>15:19:53</td>
                <td>03:20:00</td>
                <td>13:00:23</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td>01/01/2020</td>
                <td>K.S</td>
                <td>Windows</td>
                <td>DESKTOP-001</td>
                <td>13:00:23</td>
                <td>15:20:23</td>
                <td>15:19:53</td>
                <td>03:20:00</td>
                <td>13:00:23</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td>01/01/2020</td>
                <td>K.S</td>
                <td>Windows</td>
                <td>DESKTOP-001</td>
                <td>13:00:23</td>
                <td>15:20:23</td>
                <td>15:19:53</td>
                <td>03:20:00</td>
                <td>13:00:23</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td>01/01/2020</td>
                <td>K.S</td>
                <td>Windows</td>
                <td>DESKTOP-001</td>
                <td>13:00:23</td>
                <td>15:20:23</td>
                <td>15:19:53</td>
                <td>03:20:00</td>
                <td>13:00:23</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td>01/01/2020</td>
                <td>K.S</td>
                <td>Windows</td>
                <td>DESKTOP-001</td>
                <td>13:00:23</td>
                <td>15:20:23</td>
                <td>15:19:53</td>
                <td>03:20:00</td>
                <td>13:00:23</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td>01/01/2020</td>
                <td>K.S</td>
                <td>Windows</td>
                <td>DESKTOP-001</td>
                <td>13:00:23</td>
                <td>15:20:23</td>
                <td>15:19:53</td>
                <td>03:20:00</td>
                <td>13:00:23</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </tbody>
          </Table>
        </div>

        <div className="card-footer py-0">
        </div>
      </div>
    </div>
  );
}

