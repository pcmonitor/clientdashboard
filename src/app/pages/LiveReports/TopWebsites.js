import { Card } from "@material-ui/core";
import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import { Dropdown, Label, Button, Accordion } from "react-bootstrap";
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
}));

export const TopWebsites = () => {

  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleChange = panel => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <div>
      <div className="card card-custom card-stretch">
        <div className="card-header border-0 pt-6 pb-0">
          <div className="card-toolbar">
            <Dropdown className="mx-2">
              <Dropdown.Toggle variant="btn btn-outline-secondary" id="dropdown-datetype">
                Today
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item >Today</Dropdown.Item>
                <Dropdown.Item >Yesterday</Dropdown.Item>
                <Dropdown.Item >Last 7 Days</Dropdown.Item>
                <Dropdown.Item >Last 30 days</Dropdown.Item>
                <Dropdown.Divider/>
                <Dropdown.Item >Custom Range</Dropdown.Item>
                <Dropdown.Divider/>
                <Dropdown.Item >This Week</Dropdown.Item>
                <Dropdown.Item >Last Week</Dropdown.Item>
                <Dropdown.Divider/>
                <Dropdown.Item >This Month</Dropdown.Item>
                <Dropdown.Item >Last Month</Dropdown.Item>
                <Dropdown.Item >This Year</Dropdown.Item>
                <Dropdown.Item >Last Year</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>

            <Dropdown className="mx-2">
              <Dropdown.Toggle variant="btn btn-outline-secondary" id="dropdown-datetype">
                <i className="flaticon2-group"></i>All Users
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item >All Users</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <span className="mx-2 py-2">Productivity:</span>
            <Dropdown className="mx-2">
              <Dropdown.Toggle variant="btn btn-outline-secondary" id="dropdown-datetype">
                All
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item >
                  <i className="fas fa-circle text-white mr-2"></i>All</Dropdown.Item>
                <Dropdown.Item >
                  <i className="fas fa-check-circle text-success mr-2"></i>Productive</Dropdown.Item>
                <Dropdown.Item >
                  <i className="fas fa-times-circle text-danger mr-2"></i>Unproductive</Dropdown.Item>
                <Dropdown.Item >
                  <i className="fas fa-question-circle text-light mr-2"></i>Undefined</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Button variant="btn btn-outline-secondary" className="mx-2"><i className="flaticon-refresh"></i>Refresh</Button>
            <Dropdown className="mx-2">
              <Dropdown.Toggle variant="btn btn-outline-secondary" id="dropdown-datetype">
                <i className="flaticon-attachment"></i>Export
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item >Websites only</Dropdown.Item>
                <Dropdown.Item >Websites and Titles</Dropdown.Item>
                <Dropdown.Item >Websites and Subpages</Dropdown.Item>
                <Dropdown.Item >Websites and Users</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </div>
        </div>

        <div className="card-body py-0">
          <ExpansionPanel expanded={expanded === 'panel2'} onChange={handleChange('panel2')} className="my-5">
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel2bh-content"
              id="panel2bh-header"
            >
              <Typography className={classes.heading} style={{ "padding-left":"10px"}}>QUICK STATS</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Typography>
                <div className="container">
                  <div className="row">
                    <div className="mr-5">
                      <h4 className="no-margin bold">0s</h4>
                      <p>Total Web Time</p>
                    </div>
                    <div className="mx-5">
                      <h4 className="no-margin bold text-productive text-success">0s</h4>
                      <p>Productive Web Time</p>
                    </div>
                    <div className="mx-5">
                      <h4 className="no-margin bold text-light">0s</h4>
                      <p>Undefined Web Time</p>
                    </div>
                    <div className="mx-5">
                      <h4 className="no-margin bold text-danger">0s</h4>
                      <p>Unproductive Web Time</p>
                    </div>
                  </div>
                </div>
              </Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>

          <p><strong>No Result</strong></p>
          <p>Website data was not available to generate this report.</p>
          <p><span className="text-primary">Download the Agent</span> and install on the target computer to begin collecting data.</p>
        </div>

        <div className="card-footer py-0">
        </div>
      </div>
    </div>
  );
}

