import { Card } from "@material-ui/core";
import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import { Dropdown, Label, Button, Accordion } from "react-bootstrap";
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
}));

export const TopUsers = () => {

  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleChange = panel => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <div>
      <div className="card card-custom card-stretch">
        <div className="card-header border-0 pt-6 pb-0">
          <div className="card-toolbar">
            <div className="row col-12 mb-5">
              <Dropdown className="mx-2">
                <Dropdown.Toggle variant="btn btn-outline-secondary" id="dropdown-datetype">
                  Today
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item >Today</Dropdown.Item>
                  <Dropdown.Item >Yesterday</Dropdown.Item>
                  <Dropdown.Item >Last 7 Days</Dropdown.Item>
                  <Dropdown.Item >Last 30 days</Dropdown.Item>
                  <Dropdown.Divider/>
                  <Dropdown.Item >Custom Range</Dropdown.Item>
                  <Dropdown.Divider/>
                  <Dropdown.Item >This Week</Dropdown.Item>
                  <Dropdown.Item >Last Week</Dropdown.Item>
                  <Dropdown.Divider/>
                  <Dropdown.Item >This Month</Dropdown.Item>
                  <Dropdown.Item >Last Month</Dropdown.Item>
                  <Dropdown.Item >This Year</Dropdown.Item>
                  <Dropdown.Item >Last Year</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>

              <Dropdown className="mx-2">
                <Dropdown.Toggle variant="btn btn-outline-secondary" id="dropdown-datetype">
                  <i className="flaticon2-group"></i>All Users
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item >All Users</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
              <Button variant="btn btn-outline-secondary" className="mx-2"><i className="flaticon-refresh"></i>Refresh</Button>
              <Dropdown className="mx-2">
                <Dropdown.Toggle variant="btn btn-outline-secondary" id="dropdown-datetype">
                  <i className="flaticon-attachment"></i>Export
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item >Websites only</Dropdown.Item>
                  <Dropdown.Item >Websites and Titles</Dropdown.Item>
                  <Dropdown.Item >Websites and Subpages</Dropdown.Item>
                  <Dropdown.Item >Websites and Users</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </div>

            <div className="row col-12">
              <Button variant="btn btn-outline-secondary" className="active">Users</Button>
              <Button variant="btn btn-outline-secondary" className="mr-2">Groups</Button>
              <Button variant="btn btn-outline-secondary" className="ml-2">Column View</Button>
              <Button variant="btn btn-outline-secondary" className="active">Stacked View</Button>
            </div>
          </div>
        </div>

        <div className="card-body py-0">
          <div className="container px-2 my-5">
            <p><strong>No Result</strong></p>
            <p>Website data was not available to generate this report.</p>
            <p><span className="text-primary">Download the Agent</span> and install on the target computer to begin collecting data.</p>
          </div>
        </div>

        <div className="card-footer py-0">
        </div>
      </div>
    </div>
  );
}

