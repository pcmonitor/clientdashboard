import React, { Suspense, lazy } from "react";
import { Redirect, Switch, Route } from "react-router-dom";
import { LayoutSplashScreen, ContentRoute } from "../_metronic/layout";
import { BuilderPage } from "./pages/BuilderPage";
import { MyPage } from "./pages/MyPage";
import { DashboardPage } from "./pages/DashboardPage";
import { Teampulse } from "./pages/Teampulse/Teampulse";
import Screenshots from "./pages/Screenshots";
import { AccountAccess } from "./pages/Account/AccountAccess";
import { GomakeProfile } from "./pages/Account/GomakeProfile";
import { ChromebookUsers } from "./pages/Account/ChromebookUsers";
import { SecurityAudit } from "./pages/Account/SecurityAudit";
import { AccountStorage } from "./pages/Account/AccountStorage";
import { AccountUpgradePlan } from "./pages/Account/AccountUpgradePlan";
import { AccountNotifications } from "./pages/Account/AccountNotifications";

import { TopWebsites } from "./pages/LiveReports/TopWebsites";
import { TopApplications } from "./pages/LiveReports/TopApplications";
import { TopCategories } from "./pages/LiveReports/TopCategories";
import { TopUsers } from "./pages/LiveReports/TopUsers";
import { Productivity } from "./pages/LiveReports/Productivity";
import { RiskLevel } from "./pages/LiveReports/RiskLevel";
import { WorkingHours } from "./pages/LiveReports/WorkingHours";
import { ActivityLog} from "./pages/LiveReports/ActivityLog";
import { AlarmLog } from "./pages/LiveReports/AlarmLog";
import { Blocking } from "./pages/Blocking/Blocking";
import { Alarm } from "./pages/Alarm/Alarm";

import { Configuration } from "./pages/Settings/Configuration";
import { Classification } from "./pages/Settings/Classification";
import { Groups } from "./pages/Settings/Groups";
import { UserAliases } from "./pages/Settings/UserAliases";
import { ComputerAliases } from "./pages/Settings/ComputerAliases";
import { DoNotTrack } from "./pages/Settings/DoNotTrack";
import { WeeklyDigest } from "./pages/Settings/WeeklyDigest";
import { Scheduling } from "./pages/Settings/Scheduling";
import { Security } from "./pages/Settings/Security";
import { TimeZone } from "./pages/Settings/TimeZone";

export default function BasePage() {
  // useEffect(() => {
  //   console.log('Base page');
  // }, []) // [] - is required if you need only one call
  // https://reactjs.org/docs/hooks-reference.html#useeffect

  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>
        {
          /* Redirect from root URL to /dashboard. */
          <Redirect exact from="/" to="/dashboard" />
        }
        <ContentRoute path="/dashboard" component={DashboardPage} />
        <ContentRoute path="/builder" component={BuilderPage} />
        <ContentRoute path="/my-page" component={MyPage} />
        <ContentRoute path="/teampulse" component={Teampulse} />
        <ContentRoute path="/livereports/topwebsites" component={TopWebsites} />
        <ContentRoute path="/livereports/topapplications" component={TopApplications} />
        <ContentRoute path="/livereports/topcategories" component={TopCategories} />
        <ContentRoute path="/livereports/topusers" component={TopUsers} />
        <ContentRoute path="/livereports/productivity" component={Productivity} />
        <ContentRoute path="/livereports/risklevel" component={RiskLevel} />
        <ContentRoute path="/livereports/workinghours" component={WorkingHours} />
        <ContentRoute path="/livereports/activitylog" component={ActivityLog} />
        <ContentRoute path="/livereports/alarmlog" component={AlarmLog} />
        <ContentRoute path="/screenshots" component={Screenshots} />
        <ContentRoute path="/blocking" component={ Blocking } />
        <ContentRoute path="/alarm" component={ Alarm } />
        <ContentRoute path="/account/access" component={AccountAccess} />
        <ContentRoute path="/account/profile" component={GomakeProfile} />
        <ContentRoute path="/account/chromebook" component={ChromebookUsers} />
        <ContentRoute path="/account/security" component={SecurityAudit} />
        <ContentRoute path="/account/storage" component={AccountStorage} />
        <ContentRoute path="/account/upgradeplan" component={AccountUpgradePlan} />
        <ContentRoute path="/account/notification" component={AccountNotifications} />

        <ContentRoute path="/settings/configuration" component={Configuration} />
        <ContentRoute path="/settings/classification" component={Classification} />
        <ContentRoute path="/settings/groups" component={Groups} />
        <ContentRoute path="/settings/useraliases" component={UserAliases} />
        <ContentRoute path="/settings/computeraliases" component={ComputerAliases} />
        <ContentRoute path="/settings/donottrack" component={DoNotTrack} />
        <ContentRoute path="/settings/weeklydigest" component={WeeklyDigest} />
        <ContentRoute path="/settings/scheduling" component={Scheduling} />
        <ContentRoute path="/settings/security" component={Security} />
        <ContentRoute path="/settings/timezone" component={TimeZone} />
        <Redirect to="error/error-v1" />
      </Switch>
    </Suspense>
  );
}
