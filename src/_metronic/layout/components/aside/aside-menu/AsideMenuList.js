/* eslint-disable jsx-a11y/role-supports-aria-props */
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import { useLocation } from "react-router";
import { NavLink } from "react-router-dom";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl, checkIsActive } from "../../../../_helpers";

export function AsideMenuList({ layoutProps }) {
  const location = useLocation();
  const getMenuItemActive = (url, hasSubmenu = false) => {
    return checkIsActive(location, url)
      ? ` ${!hasSubmenu &&
          "menu-item-active"} menu-item-open menu-item-not-hightlighted`
      : "";
  };

  return (
    <>
      {/* begin::Menu Nav */}
      <ul className={`menu-nav ${layoutProps.ulClasses}`}>
        {/*begin::1 Level*/}
        <li
          className={`menu-item ${getMenuItemActive("/dashboard", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/dashboard">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Design/Layers.svg")} />
            </span>
            <span className="menu-text">Dashboard</span>
          </NavLink>
        </li>
        {/*end::1 Level*/}

        {/* begin::1 Level */}
        <li className={`menu-item ${getMenuItemActive("/teampulse/teampulse", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/teampulse">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Design/Layers.svg")} />
            </span>
            <span className="menu-text">Team Pulse</span>
          </NavLink>
        </li>
        {/* end::1 Level */}

        {/* begin::1 Level */}
        <li className={`menu-item menu-item-submenu ${getMenuItemActive("/livereports", true)}`}
          aria-haspopup="true"
          data-menu-toggle="hover"
        >
          <NavLink className="menu-link menu-toggle" to="/livereports">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Design/Layers.svg")} />
            </span>
            <span className="menu-text">Live Reports</span>
            <i className="menu-arrow" />
          </NavLink>

          <div className="menu-submenu">
            <i className="menu-arrow" />
            <ul className="menu-subnav">
              <li className="menu menu-item-parent" aria-haspopup="true">
                <span className="menu-link">
                  <span className="menu-text">Live Reports</span>
                </span>
              </li>

              {/* begin::2 Level */}
              <li className={`menu-item ${getMenuItemActive("/livereports/topwebsites")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/livereports/topwebsites">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Top Websites</span>
                </NavLink>
              </li>
              {/* end::2 Level */}

              {/* begin::2 Level */}
              <li className={`menu-item ${getMenuItemActive("/livereports/topapplications")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/livereports/topapplications">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Top Applications</span>
                </NavLink>
              </li>
              {/* end::2 Level */}

              {/* begin::2 Level */}
              <li className={`menu-item ${getMenuItemActive("/livereports/topcategories")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/livereports/topcategories">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Top Categories</span>
                </NavLink>
              </li>
              {/* end::2 Level */}

              {/* begin::2 Level */}
              <li className={`menu-item ${getMenuItemActive("/livereports/topusers")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/livereports/topusers">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Top Users</span>
                </NavLink>
              </li>
              {/* end::2 Level */}

              {/* begin::2 Level */}
              <li className={`menu-item ${getMenuItemActive("/livereports/productivity")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/livereports/productivity">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Productivity</span>
                </NavLink>
              </li>
              {/* end::2 Level */}

              {/* begin::2 Level */}
              <li className={`menu-item ${getMenuItemActive("/livereports/risklevel")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/livereports/risklevel">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Risk Level</span>
                </NavLink>
              </li>
              {/* end::2 Level */}

              {/* begin::2 Level */}
              <li className={`menu-item ${getMenuItemActive("/livereports/workinghours")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/livereports/workinghours">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Working hours</span>
                </NavLink>
              </li>
              {/* end::2 Level */}

              {/* begin::2 Level */}
              <li className={`menu-item ${getMenuItemActive("/livereports/activitylog")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/livereports/activitylog">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Activity Log</span>
                </NavLink>
              </li>
              {/* end::2 Level */}

              {/* begin::2 Level */}
              <li className={`menu-item ${getMenuItemActive("/livereports/alarmlog")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/livereports/alarmlog">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Alarm Log</span>
                </NavLink>
              </li>
              {/* end::2 Level */}

            </ul>
          </div>
        </li>
        {/* end::1 Level */}



        {/* begin::1 Level */}
        <li className={`menu-item ${getMenuItemActive("/screenshots", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/screenshots">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Design/Layers.svg")} />
            </span>
            <span className="menu-text">Screenshots</span>
          </NavLink>
        </li>
        {/* end::1 Level */}

        {/* begin::1 Level */}
        <li className={`menu-item ${getMenuItemActive("/blocking", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/blocking">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Design/Layers.svg")} />
            </span>
            <span className="menu-text">Blocking</span>
          </NavLink>
        </li>
        {/* end::1 Level */}

        {/* begin::1 Level */}
        {/* <li className={`menu-item ${getMenuItemActive("/alarm", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/alarm">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Design/Layers.svg")} />
            </span>
            <span className="menu-text">Alarm</span>
          </NavLink>
        </li> */}
        {/* end::1 Level */}

        {/* begin::1 Level */}
        <li className={`menu-item menu-item-submenu ${getMenuItemActive("/account", true)}`}
          aria-haspopup="true"
          data-menu-toggle="hover"
        >
          <NavLink className="menu-link menu-toggle" to="/account">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Design/Layers.svg")} />
            </span>
            <span className="menu-text">Account</span>
            <i className="menu-arrow" />
          </NavLink>

          <div className="menu-submenu">
            <i className="menu-arrow" />
            <ul className="menu-subnav">
              <li className="menu-item menu-item-parent" aria-haspopup="true">
                <span className="menu-link">
                  <span className="menu-text">Account</span>
                </span>
              </li>

              {/* begin::2 Level */}
              <li className={`menu-item ${getMenuItemActive("/account/access")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/account/access">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Access</span>
                </NavLink>
              </li>
              {/* end::2 Level */}

              {/* begin::2 Level */}
              <li className={`menu-item ${getMenuItemActive("/account/profile")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/account/profile">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Gomake Profile</span>
                </NavLink>
              </li>
              {/* end::2 Level */}

              {/* begin::2 Level */}
              <li className={`menu-item ${getMenuItemActive("/account/chromebook")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/account/chromebook">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Chromebook Users</span>
                </NavLink>
              </li>
              {/* end::2 Level */}
              {/* begin::2 Level */}
              <li className={`menu-item ${getMenuItemActive("/account/security")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/account/security">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Security Audit</span>
                </NavLink>
              </li>
              {/* end::2 Level */}
              {/* begin::2 Level */}
              <li className={`menu-item ${getMenuItemActive("/account/storage")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/account/storage">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Storage</span>
                </NavLink>
              </li>
              {/* end::2 Level */}
              {/* begin::2 Level */}
              <li className={`menu-item ${getMenuItemActive("/account/upgradeplan")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/account/upgradeplan">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Upgrade Plan</span>
                </NavLink>
              </li>
              {/* end::2 Level */}
              {/* begin::2 Level */}
              <li className={`menu-item ${getMenuItemActive("/account/notification")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/account/notification">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Notifications</span>
                </NavLink>
              </li>
              {/* end::2 Level */}
            </ul>
          </div>
        </li>
        {/* end::1 Level */}

        {/* begin::1 Level */}
        {/* <li className={`menu-item menu-item-submenu ${getMenuItemActive("/settings", true)}`}
          aria-haspopup="true"
          data-menu-toggle="hover"
        >
          <NavLink className="menu-link menu-toggle" to="/settings">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Design/Layers.svg")} />
            </span>
            <span className="menu-text">Settings</span>
            <i className="menu-arrow" />
          </NavLink>

          <div className="menu-submenu">
            <i className="menu-arrow" />
            <ul className="menu-subnav">
              <li className="menu-item menu-item-parent" aria-haspopup="true">
                <span className="menu-link">
                  <span className="menu-text">Settings</span>
                </span>
              </li>

              <li className={`menu-item ${getMenuItemActive("/settings/configuration")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/settings/configuration">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Configuration</span>
                </NavLink>
              </li>
              
              <li className={`menu-item ${getMenuItemActive("/settings/classification")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/settings/classification">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Classfication</span>
                </NavLink>
              </li>
              
              <li className={`menu-item ${getMenuItemActive("/settings/groups")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/settings/groups">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Groups</span>
                </NavLink>
              </li>
              
              <li className={`menu-item ${getMenuItemActive("/settings/useraliases")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/settings/useraliases">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">User Aliases</span>
                </NavLink>
              </li>
              
              <li className={`menu-item ${getMenuItemActive("/settings/computeraliases")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/settings/computeraliases">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Computer Aliases</span>
                </NavLink>
              </li>
              
              <li className={`menu-item ${getMenuItemActive("/settings/donottrack")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/settings/donottrack">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Do not Track</span>
                </NavLink>
              </li>
              
              <li className={`menu-item ${getMenuItemActive("/settings/weeklydigest")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/settings/weeklydigest">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Weekly Digest</span>
                </NavLink>
              </li>
              
              <li className={`menu-item ${getMenuItemActive("/settings/scheduling")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/settings/scheduling">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Scheduling</span>
                </NavLink>
              </li>
              
              <li className={`menu-item ${getMenuItemActive("/settings/security")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/settings/security">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Security</span>
                </NavLink>
              </li>

              <li className={`menu-item ${getMenuItemActive("/settings/timezone")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link" to="/settings/timezone">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Time zone</span>
                </NavLink>
              </li>

            </ul>
          </div>
        </li> */}
        {/* end::1 Level */}

        {/* begin::1 Level */}
        <li className={`menu-item ${getMenuItemActive("/logout", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/logout">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Design/Layers.svg")} />
            </span>
            <span className="menu-text">Logout</span>
          </NavLink>
        </li>
        {/* end::1 Level */}
      </ul>
      {/* end::Menu Nav */}
    </>
  );
}
